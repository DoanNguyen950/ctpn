## path = "F:/Projects/OCR/DetectLineText/detect_line/hoct_zumen/JPG/20190621_jpg"
from PIL import Image
import os
import sys

directory = "test_imgs/"


for file_name in os.listdir(directory):
  #print("Processing %s" % file_name)
  image = Image.open(os.path.join(directory, file_name))

  x,y = image.size
  new_dimensions = (1280, 768)
  output = image.resize(new_dimensions, Image.ANTIALIAS)

  output_file_name = os.path.join(directory,  file_name)
  output.save(output_file_name, "JPEG", quality = 120)

print("All done")
