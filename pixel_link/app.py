#encoding = utf-8
#       Basic libaries
from __future__ import absolute_import
import sys
import os
import shutil
import argparse
import numpy as np
import math
#       Framework
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  ###
import logging
logging.getLogger("tensorflow").setLevel(logging.WARNING)
import warnings
warnings.filterwarnings('ignore',category=FutureWarning)
warnings.filterwarnings('ignore', category=DeprecationWarning) ##
import tensorflow as tf
from tensorflow.python.ops import control_flow_ops
from tensorflow.contrib.training.python.training import evaluation
from datasets import dataset_factory
from preprocessing import ssd_vgg_preprocessing
from tf_extended import metrics as tfe_metrics
from keras.backend.tensorflow_backend import set_session
#       Files
import pixel_link
from nets import pixel_link_symbol
import util
sys.path.append('./pylib/src/')

import cv2
import plt
slim = tf.contrib.slim
import config
import time
import ntpath
#       API
from flask import Flask, jsonify, request
import requests
import base64
app = Flask(__name__)


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Text Detection')
    parser.add_argument('--checkpoint_path', dest='checkpoint_path',
                        help='the path of pretrained model to be used',
                        default="checkpoints/model.ckpt-400000", type=str)
    parser.add_argument('--gpu_memory_fraction', dest='gpu_memory_fraction',
                        help='the gpu memory fraction to be used. If less than 0, allow_growth = True is used.',
                        default=0, type=float)
    ###
    parser.add_argument('--dataset_dir', dest='dataset_dir',
                        help='The directory where the dataset files are stored.',
                        default='../data_ocrpl/test_images', type=str)
    parser.add_argument('--crop_dir', dest='crop_dir',
                        help='The directory where the crop images are stored.',
                        default='../data_ocrpl/results_detect/results_crop', type=str)
    parser.add_argument('--txt_dir', dest='txt_dir',
                        help='The directory where the text files are stored.',
                        default='../data_ocrpl/results_detect/results_txt', type=str)
    parser.add_argument('--visual_dir', dest='visual_dir',
                        help='The directory where the visualization results are stored.',
                        default='../data_ocrpl/module1_results/visualizations', type=str)
    ###
    parser.add_argument('--eval_image_width', dest='eval_image_width',
                        help='resized image width for inference',
                        default=1280, type=int)
    parser.add_argument('--eval_image_height', dest='eval_image_height',
                        help='resized image height for inference',
                        default=768, type=int)   
    parser.add_argument('--pixel_conf_threshold', dest='pixel_conf_threshold',
                        help='threshold on the pixel confidence',
                        default=0.5, type=float) 
    parser.add_argument('--link_conf_threshold', dest='link_conf_threshold',
                        help='threshold on the link confidence',
                        default=0.5, type=float) 
    parser.add_argument('--moving_average_decay', dest='moving_average_decay',
                        help='The decay rate of ExponentionalMovingAverage',
                        default=0.9999, type=float)               

    global args
    args = parser.parse_args()
    return args

def text_detection(image_path): ## 

    start_time_config = time.time()
    # cropped_dir = util.io_.mkdir(args.crop_dir)    

    config.init_config((args.eval_image_height ,args.eval_image_width), 
                       batch_size = 1, 
                       pixel_conf_threshold = args.pixel_conf_threshold,
                       link_conf_threshold = args.link_conf_threshold,
                       num_gpus = 1 
                   )

    with tf.name_scope('evaluation_%dx%d'%(args.eval_image_height, args.eval_image_width)):
        with tf.variable_scope(tf.get_variable_scope(), reuse = False):
            image = tf.placeholder(dtype=tf.int32, shape = [None, None, 3])
            image_shape = tf.placeholder(dtype = tf.int32, shape = [3, ])
            processed_image, _, _, _, _ = ssd_vgg_preprocessing.preprocess_image(image, None, None, None, None, 
                                                       out_shape = config.image_shape,
                                                       data_format = config.data_format, 
                                                       is_training = False)
            b_image = tf.expand_dims(processed_image, axis = 0)

            # build model and loss
            net = pixel_link_symbol.PixelLinkNet(b_image, is_training = False)
            masks = pixel_link.tf_decode_score_map_to_mask_in_batch(
                net.pixel_pos_scores, net.link_pos_scores)

    ### config gpu's memory         
    sess_config = tf.ConfigProto(log_device_placement = False, allow_soft_placement = True)
    if args.gpu_memory_fraction <= 0:
        sess_config.gpu_options.allow_growth = True
    elif args.gpu_memory_fraction > 0:
        sess_config.gpu_options.per_process_gpu_memory_fraction = args.gpu_memory_fraction;
    
    ### Variables to restore: moving avg. or normal weights.
    variable_averages = tf.train.ExponentialMovingAverage(
            args.moving_average_decay)
    variables_to_restore = variable_averages.variables_to_restore(
            tf.trainable_variables())
    
    ###
    results_boxes = []
    score_boxes = []
    saver = tf.train.Saver(var_list = variables_to_restore)
    with tf.Session() as sess:
        saver.restore(sess, util.tf.get_latest_ckpt(args.checkpoint_path))
        ### take image name
        _, image_name = ntpath.split(image_path)
        image_data = util.img.imread(image_path)            

        link_scores, pixel_scores, mask_vals = sess.run(
                [net.link_pos_scores, net.pixel_pos_scores, masks],
                feed_dict = {image: image_data})
        
        def get_bboxes(mask):
            return pixel_link.mask_to_bboxes(mask, image_data.shape)        
    image_idx = 0
    pixel_score = pixel_scores[image_idx, ...]
    mask = mask_vals[image_idx, ...]
    boxes = get_bboxes(mask)

    return boxes ### done
    
def cpu_process(image_path):
    ### image
    image_data = util.img.imread(image_path)
    _, image_name = ntpath.split(image_path) ## get abc.jpg
    
    image_name = image_name.replace(image_name[-4:], '')
    # print(image_name[-4:])
    org_height = int(image_data.shape[0])
    org_width = int(image_data.shape[1])
    ### txt/json files
    txt_dir = util.io_.mkdir(args.txt_dir)
    txt_name = image_name + '.txt'
    txt_path = os.path.join(txt_dir, txt_name)
    txt_file = open(txt_path, 'a')
    ### crop images
    crop_dir = util.io_.mkdir(args.crop_dir)
    crop_path = util.io_.mkdir(os.path.join(crop_dir, image_name))
    
    print ("Crop path {}".format(crop_path))

    boxes = text_detection(image_path)
    i = 0 
    info_org_img = '{"image_name": ' + '"%s"'%image_name + ', ' + '"width":'  + str(org_width) + ', ' + '"height": ' + str(org_height) + '}\n'
    txt_file.write(info_org_img)
    for bbox in boxes:
        ### top_right -> top_left -> bottom_left -> bottom_right
        values = [int(v) for v in bbox]
        x_max = max([values[0], values[2], values[4], values[6]])
        x_min = min([values[0], values[2], values[4], values[6]])
        y_max = max([values[1], values[3], values[5], values[7]])   
        y_min = min([values[1], values[3], values[5], values[7]])
        ### update coordiates
        x_max = int(x_max*org_width/1280)
        x_min = int(x_min*org_width/1280)
        y_max = int(y_max*org_height/768)
        y_min = int(y_min*org_height/768)

        h = y_max - y_min
        w = x_max - x_min

        top_left = (x_min - 7, y_min)
        bbox = [x_max, y_min, x_min, y_min, x_min, y_max, x_max, y_max]

        points = np.reshape(bbox, [4, 2])
        cnts = util.img.points_to_contours(points)
        util.img.draw_contours(image_data, contours = cnts, 
            idx = -1, color = util.img.COLOR_RGB_RED, border_width = 1)

        new_img = image_data[(y_min):y_min + h, (x_min):x_min + w]
        # tmp_1 = image_name.replace('.jpg', '')
        img_crop_name = image_name[-4:] + "_" + str(i) + '.jpg'
        img_crop_path = os.path.join(crop_path, img_crop_name)
        cv2.imwrite(img_crop_path, new_img)
        cv2.putText(image_data, '%s'%(str(i)), top_left, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 128, 255), 1, lineType=cv2.LINE_AA)
        i = i + 1

        ### txt
        txt_file = open(txt_path, 'a')
        info_crop_img = '{"image_name":'  + '"%s"'%img_crop_name + ', ' + '"id": ' + str(i) + ', ' + '"x": ' + str(x_min) + ', ' + '"y": ' + str(y_min) + ', ' + '"width": ' + str(w) + ", " + '"height": ' + str(h) + '}\n'
        txt_file.write(info_crop_img)    
    txt_file.close()
    ### draw boxes
    util.img.draw_bboxes(image_data, boxes, util.img.COLOR_RGB_RED)
    util.img.sit(image_data)

# count = 0
# @app.route("/detect", methods =["POST"])
# def detection():
#     # image_path = '/home/doannguyen950/Desktop/server_files/data_ocrpl/test_images/20190619_jpg_22_17.jpg'
#     if request.method == 'POST':
#         if request.is_json:
#             global count
#             count += 1
#             if count == 1000000:
#                 count = 0
#             ### make directory to save image sent by Postman
#             api_image_path = './api_image'
#             if not os.path.isdir(api_image_path):
#                 os.mkdir(api_image_path)
#             req = request.get_json()
#             b64_string = req[0]['b64']
#             img_data = base64.b64decode(b64_string)
#             file_name_path = os.path.join(api_image_path, 'received_image_{}.jpg'.format(str(count)))
#             # file_name_path = os.path.join(api_image_path, '20190619_jpg_22_17.jpg')
#             with open(file_name_path, 'wb') as f:
#                 f.write(img_data)
#                 print('Image is written!')
#             ### process
#             result_bbox = text_detection(file_name_path)
#             new_result_bbox = []
#             for bbox in result_bbox:
#                 bbox_list_type = list(bbox)
#                 bbox_list_type[2] = bbox_list_type[2] - bbox_list_type[0]
#                 bbox_list_type[3] = bbox_list_type[3] - bbox_list_type[1]
#                 new_result_bbox.append(bbox_list_type)
#             # result_bbox_list = list(result_bbox[0])
#             # result_bbox_list[2] = result_bbox_list[2] - result_bbox_list[0]
#             # result_bbox_list[3] = result_bbox_list[3] - result_bbox_list[1]
#             # result_bbox_tuple = tuple(result_bbox_list)
#             os.remove(file_name_path)
#             print('Received image has been removed!', file_name_path)
#             return jsonify({'bbox': str(new_result_bbox)})
#         else:
#             return 'Request was not JSON', 400

def main():
    image_path = '/home/doannguyen950/Desktop/server_files/data_ocrpl/test_images/20190619_jpg_22_17.jpg'
    args = parse_args()
    start_time_detectBoxes = time.time()
    cpu_process(image_path)
    print("---Detection:  %s seconds ---" % (time.time() - start_time_detectBoxes))
    
if __name__ == '__main__':
    # args = parse_args()
    # app.run(debug=False)
    main()
    
