"""                     Create the tfrecord files for a datsets.             
https://github.com/visipedia/tfrecords/blob/master/create_tfrecords.py
"""
from __future__ import absolute_import
#           Basic libaries
import os
import sys
import numpy as np
#           Framework
import tensorflow as tf
#           Files/folder
# sys.path.append('../')
# import config
#            Logging
import logging
logging.basicConfig(filename='convert_tfrecord.log',level=logging.DEBUG)


def _int64_feature(value):
    """Wrapper for inserting int64 features into Example proto."""
    print(value)
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def _float_feature(value):
    """Wrapper for inserting float features into Example proto."""
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def _bytes_list_feature(value):
    """Wrapper for inserting bytes list features into Example proto."""
    value = [x.encode('utf8') for x in value]
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def _bytes_feature(value):
    """Wrapper for inserting bytes features into Example proto."""
    if not isinstance(value, bytes):
        value = value.encode('utf8')
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _validate_text(text):
    """If text is not str or unicode, then try to convert it to str."""

    if isinstance(text, str):
        return text
    else:
        return str(text)

def get_list(obj: list, idx: int) -> list:
    if len(obj) > 0:
        return list(obj[:, idx])
    return []


def _convert_to_example(filename, 
                        image_buffer, 
                        labels,
                        labels_text,
                        shape: list,
                        bboxes: list,
                        oriented_bboxes: list,
                        image_format='JPEG'):
    """Build an Example proto for an example.
    Args:
        filename: an image's name
        image_buffer (image_data): string, JPEG encoding of RGB image
        height: integer, image height in pixels
        width: integer, image width in pixels
        shape: get input's shape. Maybe each image have a shape.
        bboxes: list of bbox in rectangle, [xmin, ymin, xmax, ymax]
        oriented_bboxes: list of bounding oriented boxes; each box is a list of floats in [0, 1];
            specifying [x1, y1, x2, y2, x3, y3, x4, y4]
    Returns:
      Example proto
    """

    ##
    if len(bboxes) == 0:
        logging.warning('{} has no bboxes'.format(filename))
    bboxes = np.asarray(bboxes)
    ##      
    oriented_bboxes = np.asarray(oriented_bboxes)

    example = tf.train.Example(features=tf.train.Features(feature={
        'image/shape': _int64_feature(shape),
        'image/object/bbox/xmin': _float_feature(get_list(bboxes, 0)),
        'image/object/bbox/ymin': _float_feature(get_list(bboxes, 1)),
        'image/object/bbox/xmax': _float_feature(get_list(bboxes, 2)),
        'image/object/bbox/ymax': _float_feature(get_list(bboxes, 3)),
        'image/object/bbox/x1': float_feature(get_list(oriented_bboxes, 0)),
        'image/object/bbox/y1': float_feature(get_list(oriented_bboxes, 1)),
        'image/object/bbox/x2': float_feature(get_list(oriented_bboxes, 2)),
        'image/object/bbox/y2': float_feature(get_list(oriented_bboxes, 3)),
        'image/object/bbox/x3': float_feature(get_list(oriented_bboxes, 4)),
        'image/object/bbox/y3': float_feature(get_list(oriented_bboxes, 5)),
        'image/object/bbox/x4': float_feature(get_list(oriented_bboxes, 6)),
        'image/object/bbox/y4': float_feature(get_list(oriented_bboxes, 7)),
        'image/object/bbox/label': _int64_feature(labels),
        'image/object/bbox/label_text': _bytes_feature(labels_text),
        'image/format': _bytes_feature(image_format),
        'image/filename': _bytes_feature(filename),
        'image/encoded': _bytes_feature(image_buffer)
    }))
    return example


