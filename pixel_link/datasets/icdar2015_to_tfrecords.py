#coding=utf-8
#       Basic libaries
import numpy as np
import sys
import os
#       Framework
import tensorflow as tf
#       Files
from dataset_utils import int64_feature, float_feature, bytes_feature, convert_to_example_tf1
import dataset_utils_tf2
sys.path.append('../')
import config
sys.path.append('../pylib/src')
import util
        

def cvt_to_tfrecords_tf1(output_path , data_path, gt_path):
    image_names = util.io.ls(data_path, '.jpg')#[0:10];
    print ("%d images found in %s"%(len(image_names), data_path))
    with tf.io.TFRecordWriter(output_path) as tfrecord_writer:
        for idx, image_name in enumerate(image_names):
            oriented_bboxes = [];
            bboxes = []
            labels = [];
            labels_text = [];
            path = util.io.join_path(data_path, image_name);
            print(path)
            print ("\tConverting image: %d/%d %s"%(idx, len(image_names), image_name))
            image_data = tf.compat.v1.gfile.FastGFile(path, 'rb').read(n=-1)
            # image_data = tf.io.gfile.GFile(path, 'rb').read(n=-1)
            
            
            ###         Get image's informations
            image = util.img.imread(path, rgb = True);
            ###         No resize
            shape = image.shape
            h, w, c = shape

            image_name = util.str.split(image_name, '.')[0];
            gt_name = 'gt_' + image_name + '.txt';
            gt_filepath = util.io.join_path(gt_path, gt_name);
            lines = util.io.read_lines(gt_filepath);
                
            for line in lines:
                ###
                def to_valid_range(v):
                    if v < 0:
                        return 0
                    if v > 1:
                        return 1
                    return v
		

                line = util.str.remove_all(line, '\xef\xbb\xbf')
                gt = util.str.split(line, ',');
                oriented_box = [int(gt[i]) for i in range(8)];
                oriented_box = np.asarray(oriented_box) / ([w, h] * 4);
                oriented_bboxes.append(oriented_box);
                
                xs = oriented_box.reshape(4, 2)[:, 0]                
                ys = oriented_box.reshape(4, 2)[:, 1]
                xmin = xs.min()
                xmax = xs.max()
                ymin = ys.min()
                ymax = ys.max()
                bboxes.append([xmin, ymin, xmax, ymax])
                # print(bboxes) ###

                # might be wrong here, but it doesn't matter because the label is not going to be used in detection
                labels_text.append(gt[-1]); 
                ignored = util.str.contains(gt[-1], '###')
                if ignored:
                    labels.append(config.ignore_label);
                else:
                    labels.append(config.text_label)
            example = convert_to_example_tf1(image_data, image_name, labels, labels_text, bboxes, oriented_bboxes, shape)
            tfrecord_writer.write(example.SerializeToString())

def cvt_to_tfrecords_tf2(output_path , data_path, gt_path):
    image_names = util.io.ls(data_path, '.jpg')#[0:10];
    print ("%d images found in %s"%(len(image_names), data_path))
    with tf.io.TFRecordWriter(output_path) as tfrecord_writer:
        for idx, image_name in enumerate(image_names):
            oriented_bboxes = [];
            bboxes = []
            labels = [];
            labels_text = [];
            path = util.io.join_path(data_path, image_name);
            print(path)
            print ("\tConverting image: %d/%d %s"%(idx, len(image_names), image_name))
            image_data = tf.io.gfile.GFile(path, 'rb').read(n=-1)
            
            ###         Get image's informations
            image = util.img.imread(path, rgb = True)
            ###         No resize
            shape = image.shape
            h, w, c = shape

            image_name = util.str.split(image_name, '.')[0];
            gt_name = 'gt_' + image_name + '.txt';
            gt_filepath = util.io.join_path(gt_path, gt_name);
            lines = util.io.read_lines(gt_filepath);
                
            for line in lines:
                ###
                def to_valid_range(v):
                    if v < 0:
                        return 0
                    if v > 1:
                        return 1
                    return v
		

                line = util.str.remove_all(line, '\xef\xbb\xbf')
                gt = util.str.split(line, ',');
                oriented_box = [int(gt[i]) for i in range(8)];
                oriented_box = np.asarray(oriented_box) / ([w, h] * 4);
                oriented_bboxes.append(oriented_box);
                
                xs = oriented_box.reshape(4, 2)[:, 0]                
                ys = oriented_box.reshape(4, 2)[:, 1]
                xmin = xs.min()
                xmax = xs.max()
                ymin = ys.min()
                ymax = ys.max()
                bboxes.append([xmin, ymin, xmax, ymax])
                # print(bboxes) ###

                # might be wrong here, but it doesn't matter because the label is not going to be used in detection
                labels_text.append(gt[-1]); 
                ignored = util.str.contains(gt[-1], '###')
                if ignored:
                    labels.append(config.ignore_label);
                else:
                    labels.append(config.text_label)
            example = dataset_utils_tf2._convert_to_example(image_data, image_name, labels, labels_text, bboxes, oriented_bboxes, shape)
            tfrecord_writer.write(example.SerializeToString())

        
if __name__ == "__main__":
    # root_dir = util.io.get_absolute_path(config.RAW_DATASETS_PATH)
    # output_dir = util.io.get_absolute_path(config.TFRECORD_DATASET_PATH)

    training_data_dir = util.io.join_path(root_dir, 'images')
    training_gt_dir = util.io.join_path(root_dir,'labels_gt')
    cvt_to_tfrecords_tf1(output_path = util.io.join_path(output_dir, 'dataset_ocr_train.tfrecord'), data_path = training_data_dir, gt_path = training_gt_dir)
    # cvt_to_tfrecords_tf2(output_path = util.io.join_path(output_dir, 'dataset_ocr_train.tfrecord'), data_path = training_data_dir, gt_path = training_gt_dir)

    test_data_dir = util.io.join_path(root_dir, 'test_images')
    test_gt_dir = util.io.join_path(root_dir,'test_labels_gt')
    cvt_to_tfrecords_tf1(output_path = util.io.join_path(output_dir, 'dataset_ocr_test.tfrecord'), data_path = test_data_dir, gt_path = test_gt_dir)
    # cvt_to_tfrecords_tf2(output_path = util.io.join_path(output_dir, 'dataset_ocr_test.tfrecord'), data_path = test_data_dir, gt_path = test_gt_dir)
